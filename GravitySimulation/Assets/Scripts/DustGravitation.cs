﻿using System.Linq;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateInGroup(typeof(SimulationSystemGroup))]
public partial class DustGravitation : SystemBase
{
    EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;
    EntityQuery positionMassQuery;
    float mergingActiveDelay = 5f;

    protected override void OnCreate()
    {
        endSimulationEntityCommandBufferSystem = World.GetExistingSystem<EndSimulationEntityCommandBufferSystem>();
        positionMassQuery = GetEntityQuery(
            ComponentType.ReadOnly<LocalToWorld>(),
            ComponentType.ReadOnly<Mass>()
        );
    }

    protected override void OnUpdate()
    {
        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer();

        var dustEntities = positionMassQuery.ToEntityArray(Allocator.Persistent);
        var localToWorldFromEntity = GetComponentDataFromEntity<LocalToWorld>();
        var translationFromEntity = GetComponentDataFromEntity<Translation>();
        var scaleFromEntity = GetComponentDataFromEntity<Scale>();
        var massFromEntity = GetComponentDataFromEntity<Mass>();
        var velocityFromEntity = GetComponentDataFromEntity<Velocity>();
        var isNextToFromEntity = GetComponentDataFromEntity<IsNextTo>();

        var gravitationInfluence = new GravitationInfluenceJob
        {
            OtherEntities = dustEntities,
            TranslationFromEntity = translationFromEntity,
            MassFromEntity = massFromEntity,
        };

        var researchNeighbour = new ResearchNeighbour
        {
            OtherEntities = dustEntities,
            TranslationFromEntity = translationFromEntity,
            ScaleFromEntity = scaleFromEntity,
            IsNextToFromEntity = isNextToFromEntity
        };

        var neighbourCleanUp = new IsNextToCleanUpJob
        {
            LocalToWorldFromEntity = localToWorldFromEntity
        };

        var dustMerging = new DustMergingJob
        {
            MassFromEntity = massFromEntity,
            VelocityFromEntity = velocityFromEntity,
            ScaleFromEntity = scaleFromEntity,
            EntityCommandBuffer = ecb
        };

        var gravitationInfluenceJobHandle = gravitationInfluence.ScheduleParallel(Dependency);
        Dependency = JobHandle.CombineDependencies(Dependency, gravitationInfluenceJobHandle);

        JobHandle researchNeighbourJobHandle;
        JobHandle dustMergingJobHandle;
        JobHandle neighbourCleanUpJobHandle = default;
        var isDustMergingActive = Time.ElapsedTime > mergingActiveDelay;
        if (isDustMergingActive)
        {
            researchNeighbourJobHandle = researchNeighbour.ScheduleParallel(Dependency);
            dustMergingJobHandle = dustMerging.Schedule(researchNeighbourJobHandle);
            neighbourCleanUpJobHandle = neighbourCleanUp.ScheduleParallel(dustMergingJobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, neighbourCleanUpJobHandle);
            endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        gravitationInfluenceJobHandle.Complete();
        if (isDustMergingActive) neighbourCleanUpJobHandle.Complete();
        dustEntities.Dispose();
    }
}

[BurstCompile]
public partial struct GravitationInfluenceJob : IJobEntity
{
    [ReadOnly]
    [NativeDisableContainerSafetyRestriction]
    [NativeDisableParallelForRestriction]
    public ComponentDataFromEntity<Translation> TranslationFromEntity;

    [ReadOnly]
    [NativeDisableContainerSafetyRestriction]
    [NativeDisableParallelForRestriction]
    public ComponentDataFromEntity<Mass> MassFromEntity;

    [ReadOnly] public NativeArray<Entity> OtherEntities;

    public void Execute(ref Velocity velocity, in Dust dust, in Translation translation, in Mass mass)
    {
        foreach (var other in OtherEntities)
        {
            var otherTranslation = TranslationFromEntity[other];
            var otherMass = MassFromEntity[other];
            var dir = translation.Value - otherTranslation.Value;
            var distance = math.lengthsq(dir);

            if (distance == 0) continue;
            var G = 6.67f * math.pow(10, -11);
            var gravitationBoost = 2000000f;
            var gravityForce = G * mass.Value * otherMass.Value / distance;
            velocity.Value += -dir * gravityForce * gravitationBoost;
        }
    }
}

public struct IsNextTo : IComponentData
{
    public Entity Value;

    public static implicit operator IsNextTo(Entity entity)
    {
        return new IsNextTo() { Value = entity };
    }
}

[BurstCompile]
public partial struct ResearchNeighbour : IJobEntity
{
    [ReadOnly] public NativeArray<Entity> OtherEntities;

    [ReadOnly]
    [NativeDisableContainerSafetyRestriction]
    [NativeDisableParallelForRestriction]
    public ComponentDataFromEntity<Translation> TranslationFromEntity;

    [ReadOnly]
    [NativeDisableContainerSafetyRestriction]
    [NativeDisableParallelForRestriction]
    public ComponentDataFromEntity<IsNextTo> IsNextToFromEntity;

    [ReadOnly]
    [NativeDisableContainerSafetyRestriction]
    [NativeDisableParallelForRestriction]
    public ComponentDataFromEntity<Scale> ScaleFromEntity;

    public void Execute(Entity entity, ref IsNextTo isNextTo, in Translation translation)
    {
        if (IsNextToFromEntity.HasComponent(isNextTo.Value)) return;
        foreach (var other in OtherEntities)
        {
            if (other == entity) continue;
            var otherTranslation = TranslationFromEntity[other];
            var dir = translation.Value - otherTranslation.Value;
            var distance = math.lengthsq(dir);
            var threshold = ScaleFromEntity.HasComponent(entity) && ScaleFromEntity.HasComponent(other) ?
                            ScaleFromEntity[entity].Value / 2f + ScaleFromEntity[other].Value / 2f :
                            1f;
            if (distance >= threshold || IsNextToFromEntity[other].Value == entity)
            {
                continue;
            }
            isNextTo = other;
            break;
        }
    }
}

public partial struct DustMergingJob : IJobEntity
{
    [NativeDisableContainerSafetyRestriction]
    [NativeDisableParallelForRestriction]
    public ComponentDataFromEntity<Mass> MassFromEntity;

    [NativeDisableContainerSafetyRestriction]
    [NativeDisableParallelForRestriction]
    public ComponentDataFromEntity<Velocity> VelocityFromEntity;

    [NativeDisableContainerSafetyRestriction]
    [NativeDisableParallelForRestriction]
    public ComponentDataFromEntity<Scale> ScaleFromEntity;

    public EntityCommandBuffer EntityCommandBuffer;

    public void Execute(Entity entity, ref IsNextTo isNextTo)
    {
        var other = isNextTo.Value;
        if (!MassFromEntity.HasComponent(other)
            || !VelocityFromEntity.HasComponent(other)
            || !ScaleFromEntity.HasComponent(other))
        {
            isNextTo.Value = Entity.Null;
            return;
        }

        var otherMass = MassFromEntity[other];
        var otherVelocity = VelocityFromEntity[other];
        var otherScale = ScaleFromEntity[other];
        MassFromEntity[entity] = new Mass() { Value = MassFromEntity[entity].Value + otherMass.Value };
        VelocityFromEntity[entity] = new Velocity() { Value = math.length(VelocityFromEntity[entity].Value + otherVelocity.Value) > 1000f ? new float3(1000f) : (VelocityFromEntity[entity].Value + otherVelocity.Value) };
        ScaleFromEntity[entity] = new Scale() { Value = math.max(ScaleFromEntity[entity].Value, otherScale.Value) + 1f };

        EntityCommandBuffer.DestroyEntity(other);
        isNextTo.Value = Entity.Null;
    }
}

public partial struct IsNextToCleanUpJob : IJobEntity
{
    [ReadOnly]
    [NativeDisableContainerSafetyRestriction]
    [NativeDisableParallelForRestriction]
    public ComponentDataFromEntity<LocalToWorld> LocalToWorldFromEntity;

    public void Execute(ref IsNextTo isNextTo)
    {
        if (!LocalToWorldFromEntity.HasComponent(isNextTo.Value))
        {
            isNextTo.Value = Entity.Null;
        }
    }
}