﻿using UnityEngine;

[CreateAssetMenu(fileName = "SpawningData", menuName = "Data/SpawningData")]
public class DustSpawnerData : ScriptableObject
{
    public float InitialRadius;
    public int Count;
    public int CountPerSpawn;
    public float SpawnDuration;
    public float ExplosionForce;
}
