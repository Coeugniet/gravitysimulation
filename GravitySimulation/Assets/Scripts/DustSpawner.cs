﻿using System.Diagnostics;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Random = Unity.Mathematics.Random;

public partial class DustSpawning : SystemBase
{
    [BurstCompile]
    struct SetDustPosition : IJobParallelFor
    {
        [NativeDisableContainerSafetyRestriction]
        [NativeDisableParallelForRestriction]
        public ComponentDataFromEntity<Translation> TranslationFromEntity;

        [NativeDisableContainerSafetyRestriction]
        [NativeDisableParallelForRestriction]
        public ComponentDataFromEntity<Rotation> RotationFromEntity;

        public NativeArray<Entity> Entities;
        public float3 Center;
        public float Radius;

        public void Execute(int i)
        {
            var entity = Entities[i];
            var random = new Random(((uint)(entity.Index + i + 1) * 0x9F6ABC1));
            var dir = math.normalizesafe(random.NextFloat3() - new float3(0.5f, 0.5f, 0.5f));
            var pos = Center + (dir * Radius);
            
            var translation = new Translation
            {
                Value = pos
            };
            
            var rotation = new Rotation
            {
                Value = quaternion.LookRotationSafe(dir, math.up())
            };
            
            TranslationFromEntity[entity] = translation;
            RotationFromEntity[entity] = rotation;
        }
    }

    [BurstCompile]
    struct SetDustInitialVelocity : IJobParallelFor
    {
        [NativeDisableContainerSafetyRestriction]
        [NativeDisableParallelForRestriction]
        public ComponentDataFromEntity<Translation> TranslationFromEntity;

        [NativeDisableContainerSafetyRestriction]
        [NativeDisableParallelForRestriction]
        public ComponentDataFromEntity<Velocity> VelocityFromEntity;

        public NativeArray<Entity> Entities;
        public float3 Center;
        public float ExplosionForce;

        public void Execute(int i)
        {
            var entity = Entities[i];
            var velocity = VelocityFromEntity[entity];
            var shift = float3.zero;
            var pos = TranslationFromEntity[entity].Value;

            velocity.Value = (pos - Center + shift) * ExplosionForce;
            VelocityFromEntity[entity] = velocity;
        }
    }

    protected override void OnUpdate()
    {
        Entities.WithStructuralChanges().ForEach((Entity entity, ref DustSpawner spawner, in LocalToWorld localToWorld) =>
        {
            if (spawner.TargetCount <= 0 || spawner.Count >= spawner.TargetCount) EntityManager.DestroyEntity(entity);

            var spawnDelay = spawner.CountPerSpawn * spawner.SpawnDuration / spawner.TargetCount;
            if (Time.ElapsedTime < spawner.LastSpawn + spawnDelay)
            {
                return;
            }

            spawner.LastSpawn = Time.ElapsedTime;
            var countToSpawn = spawner.Count + spawner.CountPerSpawn > spawner.TargetCount ?
                               spawner.TargetCount - spawner.Count :
                               spawner.CountPerSpawn;

            var dustEntities = CollectionHelper.CreateNativeArray<Entity, RewindableAllocator>(countToSpawn, ref World.Unmanaged.UpdateAllocator);
            spawner.Count += countToSpawn;

            // Dusts instantiation
            EntityManager.Instantiate(spawner.Prefab, dustEntities);
            
            var localToWorldFromEntity = GetComponentDataFromEntity<LocalToWorld>();
            var velocityFromEntity = GetComponentDataFromEntity<Velocity>();
            var translationFromEntity = GetComponentDataFromEntity<Translation>();
            var rotationFromEntity = GetComponentDataFromEntity<Rotation>();

            // Set position parallel job
            var setDustPositionJob = new SetDustPosition
            {
                TranslationFromEntity = translationFromEntity,
                RotationFromEntity = rotationFromEntity,
                Entities = dustEntities,
                Center = localToWorld.Position,
                Radius = spawner.InitialRadius,
            };

            var t = 1 - (spawner.Count / (float)(spawner.TargetCount != 0 ? spawner.TargetCount : 1));
            var explosionForce = math.lerp(spawner.ExplosionForce * 0.1f, spawner.ExplosionForce, t);

            // Set initial velocity parallel job
            var setDustInitialVelocityJob = new SetDustInitialVelocity
            {
                TranslationFromEntity = translationFromEntity,
                VelocityFromEntity = velocityFromEntity,
                Entities = dustEntities,
                Center = localToWorld.Position,
                ExplosionForce = explosionForce
            };

            var setDustPositionJobHandle = setDustPositionJob.Schedule(countToSpawn, 64, Dependency);
            var setDustInitialVelocityJobHandle = setDustInitialVelocityJob.Schedule(countToSpawn, 64, setDustPositionJobHandle);
            Dependency = JobHandle.CombineDependencies(setDustPositionJobHandle, setDustInitialVelocityJobHandle , Dependency);
        }).Run();
    }
}
