﻿using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public struct Velocity : IComponentData
{
    public float3 Value;
}

public struct Mass : IComponentData
{
    public float Value;
}

public partial class ForceSystem : SystemBase
{
    [BurstCompile]
    public partial struct ComputeVelocity : IJobEntity
    {
        public float DeltaTime;

        void Execute(ref Translation translation, ref Velocity velocity, in Mass mass)
        {
            var movement = (velocity.Value / mass.Value) * DeltaTime;
            translation.Value += movement;
            velocity.Value -= movement;
        }
    }

    protected override void OnUpdate()
    {
        var ComputeVelocityJob = new ComputeVelocity { DeltaTime = Time.DeltaTime };
        Dependency = ComputeVelocityJob.ScheduleParallel(Dependency);
    }
}
