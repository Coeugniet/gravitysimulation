using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public struct DustSpawner : IComponentData
{
    public Entity Prefab;
    public int Count;
    public int TargetCount;
    public int CountPerSpawn;
    public float SpawnDuration;
    public float InitialRadius;
    public double LastSpawn;
    public float ExplosionForce;
}

public class DustSpawnerAuthoring : MonoBehaviour, IDeclareReferencedPrefabs
{
    public GameObject Prefab;
    public float InitialRadius;
    public int Count;
    public int CountPerSpawn;
    public float SpawnDuration;
    public DustSpawnerData Data;

    public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
    {
        referencedPrefabs.Add(Prefab);
    }
}

public class DustSpawnerConversion : GameObjectConversionSystem
{
    protected override void OnUpdate()
    {
        Entities.ForEach((DustSpawnerAuthoring authoring) =>
        {
            var entity = GetPrimaryEntity(authoring);
            DstEntityManager.AddComponentData(entity, new DustSpawner()
            {
                Prefab = GetPrimaryEntity(authoring.Prefab),
                TargetCount = authoring.Data.Count,
                InitialRadius = authoring.Data.InitialRadius,
                CountPerSpawn = authoring.Data.CountPerSpawn,
                SpawnDuration = authoring.Data.SpawnDuration,
                ExplosionForce = authoring.Data.ExplosionForce,
            });
        });
    }
}
