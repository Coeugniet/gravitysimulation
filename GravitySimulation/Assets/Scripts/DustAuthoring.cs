﻿using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public struct Dust : IComponentData
{

}

public class DustAuthoring : MonoBehaviour
{

}

public class DustConversion : GameObjectConversionSystem
{
    protected override void OnUpdate()
    {
        Entities.ForEach((DustAuthoring authoring) =>
        {
            var entity = GetPrimaryEntity(authoring);

            DstEntityManager.AddComponent<Dust>(entity) ;
            DstEntityManager.AddComponent<Velocity>(entity);
            DstEntityManager.AddComponent<IsNextTo>(entity);
            DstEntityManager.AddComponent<TargetableByRay>(entity);

            DstEntityManager.AddComponentData(entity, new Scale()
            { 
                Value = 1f
            });

            DstEntityManager.AddComponentData(entity, new Mass()
            {
                Value = 1f
            });
        });
    }
}