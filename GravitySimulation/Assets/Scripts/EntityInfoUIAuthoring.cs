﻿using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class EntityInfoUIAuthoring : MonoBehaviour
{
    public TMPro.TextMeshProUGUI UI;
}

public class EntityInfoUIConversion : GameObjectConversionSystem
{
    protected override void OnUpdate()
    {
        Entities.ForEach((EntityInfoUIAuthoring authoring) =>
        {
            var entity = GetPrimaryEntity(authoring);
            DstEntityManager.AddComponentData(entity, new EntityInfoUI()
            {
                Data = authoring.UI
            });
        });
    }
}

public class EntityInfoUI : IComponentData
{
    public TMPro.TextMeshProUGUI Data;
}

[UpdateInGroup(typeof(LateSimulationSystemGroup))]
public partial class UpdatingDustUI : SystemBase
{
    Entity UIEntity;
    EntityInfoUI UIComponent;

    protected override void OnStartRunning()
    {
        RequireSingletonForUpdate<EntityInfoUI>();
        UIEntity = GetSingletonEntity<EntityInfoUI>();
        UIComponent = EntityManager.GetComponentData<EntityInfoUI>(UIEntity);
    }

    protected override void OnUpdate()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        var rayLength = 1000000f;

        var detectingRayIntersectionJob = new DetectingRayIntersection
        {
            lineStart = ray.origin,
            lineEnd = ray.origin + ray.direction * rayLength
        };

        Dependency = detectingRayIntersectionJob.ScheduleParallel(Dependency);
        Dependency.Complete();

        var nearestCrossedByRaycastEntity = Entity.Null;
        var actualDistance = 0f;

        Entities.WithAll<Mass>().ForEach((Entity entity, in TargetableByRay targetableByRay) =>
        {
            if (!targetableByRay.IsCrossed) return;
            if (HasComponent<Translation>(entity))
            {
                var translation = GetComponent<Translation>(entity).Value;
                var cameraPos = new float3(Camera.main.transform.position);
                if (nearestCrossedByRaycastEntity == Entity.Null
                    || actualDistance > math.lengthsq(translation - cameraPos))
                {
                    actualDistance = math.lengthsq(translation - cameraPos);
                    nearestCrossedByRaycastEntity = entity;
                }
            }
        }).WithoutBurst().Run();

        if (nearestCrossedByRaycastEntity == Entity.Null)
        {
            Debug.DrawLine(ray.origin, ray.origin + ray.direction * rayLength, Color.red);
        } else
        {
            Debug.DrawLine(ray.origin, ray.origin + ray.direction * rayLength, Color.green);
            UIComponent.Data.SetText($"object {nearestCrossedByRaycastEntity.Index} - mass {GetComponent<Mass>(nearestCrossedByRaycastEntity).Value} kg - speed {math.length(GetComponent<Velocity>(nearestCrossedByRaycastEntity).Value)} m/s");
        }
    }
}

[BurstCompile]
public partial struct DetectingRayIntersection : IJobEntity
{
    public float3 lineStart;
    public float3 lineEnd;

    void Execute(ref TargetableByRay targetableByRay, in Scale scale, in Translation translation)
    {
        bool isCrossed = IsLineIntersectingSphere(lineStart.x,
                                                  lineStart.y,
                                                  lineStart.z,
                                                  lineEnd.x,
                                                  lineEnd.y,
                                                  lineEnd.z,
                                                  translation.Value.x,
                                                  translation.Value.y,
                                                  translation.Value.z,
                                                  scale.Value / 2f);
        targetableByRay.IsCrossed = isCrossed;
    }

    bool IsLineIntersectingSphere(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3, float r)
    {
        float a = math.pow(x2 - x1, 2) + math.pow(y2 - y1, 2) + math.pow(z2 - z1, 2);
        float b = 2 * ((x2 - x1) * (x1 - x3) + (y2 - y1) * (y1 - y3) + (z2 - z1) * (z1 - z3));
        float c = math.pow(x3, 2) + math.pow(y3, 2) + math.pow(z3, 2) + math.pow(x1, 2) + math.pow(y1, 2) + math.pow(z1, 2)
                  - 2 * (x3 * x1 + y3 * y1 + z3 * z1) - math.pow(r, 2);
        return (b * b - 4 * a * c) >= 0;
    }
}

public struct TargetableByRay : IComponentData
{
    public bool IsCrossed;
}
